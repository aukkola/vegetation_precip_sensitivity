#!/bin/bash

#Processes monthly max NDVI3g v1.1 data
#Calculates annual max and mean for financial and calendar years to account
#for boreal/austral summer during 1981-2017
#Also regrids to GPCC and CRU precip resolution and extracts same time periods for 
#precipitation


#Working directory
INDIR="/srv/ccrc/data04/z3509830/Nutrient_constraint/"

#Location of NDVI data
ndvi_dir=$INDIR"/Annual_obs_data_new_NDVI/ndvi/"

#Input NDVI file (can't work out how to break line...)
infile_ndvi=$ndvi_dir"/monthly_max/ndvi3g_geo_v1_1_1981to2017_mergetime_ndviMonMax.nc"
              

#Output directory
outdir_max=$ndvi_dir"/annual_max/"
outdir_mean=$ndvi_dir"/annual_mean/"

mkdir -p $outdir_max $outdir_mean

#Esoil directory
esoil_dir=$INDIR"/Annual_obs_data_new_NDVI/Esoil/"
mkdir -p $esoil_dir

#PET directory
pet_dir=$INDIR"/Annual_obs_data_new_NDVI/PET/"
mkdir -p $pet_dir




#Get precip and Esoil files for regridding
cru_file=($INDIR"/../Obs_precip_products/CRU_TS4.02/Data_1981_2017/"\
"CRU_TS4.02_monthly_total_precipitation_1981_2017.nc")

gpcc_file=($INDIR"/../Obs_precip_products/GPCC/Data_1981_2016/"\
"GPCC_total_monthly_precipitation_v2018_025_1981_2016.nc")

esoil_file=($INDIR"//Esoil_data/Processed_data/"\
"Monthly_soil_evaporation_MSWEP_1980_2015.nc")

pet_file=($INDIR"/../Obs_Tair_SWdown_products/CRU_TS4.02/Data_1901_2017/"\
"PriestleyTaylor_PET/CRU_TS4.02_monthly_PriestleyTaylor_PET_mm_month_1901_2017.nc")


####################
### QC mask data ###
####################

#First apply Arden's QC mask to the monthly max data

infile_ndvi_masked=$ndvi_dir"/monthly_max/ndvi3g_geo_v1_1_1981to2017_mergetime_ndviMonMax_masked.nc"
cdo div $infile_ndvi -gec,0.5 $INDIR"/QC_mask/QCmask_1101GLOBAL_1982_2015.nc" $infile_ndvi_masked



###################
### Regrid data ###
###################

#First regrid NDVI and ESoil data. For NDVI, need to find the annual max as well 
#as when it occurs. Feels more correct to regrid first as probably shouldn't 
#regrid max. indices. Also select years to match precip datasets

### CRU ###

#NDVI
out_temp_cru=$ndvi_dir"/monthly_max/Mon_max_temp_regrid_cru.nc"
cdo selyear,1981/2017 -remapycon,$cru_file $infile_ndvi_masked $out_temp_cru

#Esoil
out_temp_esoil_cru=$esoil_dir"/Total_monthly_Esoil_CRU_res_1981_2015.nc"
cdo selyear,1981/2015 -remapycon,$cru_file $esoil_file $out_temp_esoil_cru

#PET
out_temp_pet_cru=$pet_dir"/Total_monthly_PET_CRU_res_1981_2017.nc"
cdo selyear,1981/2017 -remapycon,$cru_file $pet_file $out_temp_pet_cru



### GPCC ###

#NDVI
out_temp_gpcc=$ndvi_dir"/monthly_max/Mon_max_temp_regrid_gpcc.nc"
cdo -L selyear,1981/2016 -remapycon,$gpcc_file $infile_ndvi_masked $out_temp_gpcc

#Esoil
out_temp_esoil_gpcc=$esoil_dir"/Total_monthly_Esoil_GPCC_res_1981_2015.nc"
cdo selyear,1981/2015 -remapycon,$gpcc_file $esoil_file $out_temp_esoil_gpcc

#PET
out_temp_pet_gpcc=$pet_dir"/Total_monthly_PET_GPCC_res_1981_2016.nc"
cdo -b F64 selyear,1981/2016 -remapycon,$gpcc_file $pet_file $out_temp_pet_gpcc



#####################
### Calendar year ###
#####################


### Annual maximum ###

#Select complete years 1982-2017

#CRU
out_cal_max_cru=$outdir_max"/Max_annual_NDVI_v3.1.1_1982_2017_cal_year_CRU_res.nc"
cdo yearmax -selyear,1982/2017 $out_temp_cru $out_cal_max_cru

#GPCC (ends in 2016)
out_cal_max_gpcc=$outdir_max"/Max_annual_NDVI_v3.1.1_1982_2016_cal_year_GPCC_res.nc"
cdo yearmax -selyear,1982/2016 $out_temp_gpcc $out_cal_max_gpcc


#Find indices for maximums

#Use R as not sure if this can be done in cdo

#Create script
cat > calc_annual_ind.R << EOF

  library(raster)

  #function to deal with NA
  which_max <- function(x) {
    if(all(is.na(x))) { return(NA) } else { return(which.max(x)) }
  }

  ### CRU ###
  
  #Use monthly file
  data_cru <- brick("$out_temp_cru")

  #Loop through years (start from 7, i.e. Jan 1982)
  ind <- seq(7, by=12, length.out=floor(nlayers(data_cru)/12))

  max_ind_cru <- brick(lapply(ind, function(x) calc(data_cru[[x:(x+11)]], which_max)))

  outfile <- paste0("$outdir_max", "/Max_annual_index_NDVI_v3.1.1_", 
                    "1982_2017_cal_year_CRU_res.nc")
                    
  writeRaster(max_ind_cru, outfile, varname="month_index", 
              zunit="Years since 01-01-1982", overwrite=TRUE)


  ### GPCC ###
  
  data_gpcc <- brick("$out_temp_gpcc")

  ind <- seq(7, by=12, length.out=floor(nlayers(data_gpcc)/12))

  max_ind_gpcc <- brick(lapply(ind, function(x) calc(data_gpcc[[x:(x+11)]], which_max)))

  outfile <- paste0("$outdir_max", "/Max_annual_index_NDVI_v3.1.1_", 
                    "1982_2016_cal_year_GPCC_res.nc")
                    
  writeRaster(max_ind_gpcc, outfile, varname="month_index", 
              zunit="Years since 01-01-1982", overwrite=TRUE)

EOF

Rscript calc_annual_ind.R

rm calc_annual_ind.R


### Annual mean ###

#CRU
out_cal_mean_cru=$outdir_mean"/Mean_annual_NDVI_v3.1.1_1982_2017_cal_year_CRU_res.nc"
cdo yearmean -selyear,1982/2017 $out_temp_cru $out_cal_mean_cru

#GPCC
out_cal_mean_gpcc=$outdir_mean"/Mean_annual_NDVI_v3.1.1_1982_2016_cal_year_GPCC_res.nc"
cdo yearmean -selyear,1982/2016 $out_temp_gpcc $out_cal_mean_gpcc



### Annual total precipitation ###

outdir_pr=$INDIR"/Annual_obs_data_new_NDVI/precip/"

#CRU
out_cal_pr_cru=$outdir_pr"/Total_annual_CRU_precipitation_1982_2017_cal_year.nc"
cdo yearsum -selyear,1982/2017 $cru_file $out_cal_pr_cru

#GPCC
out_cal_pr_gpcc=$outdir_pr"/Total_annual_GPCC_precipitation_1982_2016_cal_year.nc"
cdo yearsum -selyear,1982/2016 $gpcc_file $out_cal_pr_gpcc


### Annual total Esoil ###

#CRU
out_cal_esoil_cru=$esoil_dir"/Total_annual_ESoil_CRU_res_1982_2015_cal_year.nc"
cdo yearsum -selyear,1982/2015 $out_temp_esoil_cru $out_cal_esoil_cru

#GPCC
out_cal_esoil_gpcc=$esoil_dir"/Total_annual_ESoil_GPCC_res_1982_2015_cal_year.nc"
cdo yearsum -selyear,1982/2015 $out_temp_esoil_gpcc $out_cal_esoil_gpcc


### Annual total PET ###

#CRU
out_cal_pet_cru=$pet_dir"/Total_annual_PET_CRU_res_1982_2017_cal_year.nc"
cdo yearsum -selyear,1982/2017 $out_temp_pet_cru $out_cal_pet_cru

#GPCC
out_cal_pet_gpcc=$pet_dir"/Total_annual_PET_GPCC_res_1982_2016_cal_year.nc"
cdo yearsum -selyear,1982/2016 $out_temp_pet_gpcc $out_cal_pet_gpcc



######################
### Financial year ###
######################


### Annual maximum ###

#Select complete years Jul 1981 - Jul 2017

#CRU
out_fin_max_cru=$outdir_max"/Max_annual_NDVI_v3.1.1_1981_2016_fin_year_CRU_res.nc"
cdo -L selyear,1981/2016 -timselmax,12 $out_temp_cru $out_fin_max_cru

#GPCC (needs to end in 2015/2016)
out_fin_max_gpcc=$outdir_max"/Max_annual_NDVI_v3.1.1_1981_2015_fin_year_GPCC_res.nc"
cdo -L selyear,1981/2015 -timselmax,12 $out_temp_gpcc $out_fin_max_gpcc


#Find indices for maximums

#Use R as not sure if this can be done in cdo

#Create script
cat > calc_annual_ind.R << EOF

  library(raster)

  #function to deal with NA
  which_max <- function(x) {
    if(all(is.na(x))) { return(NA) } else { return(which.max(x)) }
  }

  ### CRU ###
  
  #Use monthly file
  data_cru <- brick("$out_temp_cru")

  #Loop through years (start from 1, i.e. Jul 1981)
  ind <- seq(1, by=12, length.out=floor(nlayers(data_cru)/12))

  max_ind_cru <- brick(lapply(ind, function(x) calc(data_cru[[x:(x+11)]], which_max)))

  outfile <- paste0("$outdir_max", "/Max_annual_index_NDVI_v3.1.1_", 
                    "1981_2016_fin_year_CRU_res.nc")
                    
  writeRaster(max_ind_cru, outfile, varname="month_index", 
              zunit="Years since 01-07-1981", overwrite=TRUE)


  ### GPCC ###
  
  data_gpcc <- brick("$out_temp_gpcc")
  
  #Loop through years (start from 1, i.e. Jul 1981)
  ind <- seq(1, by=12, length.out=floor(nlayers(data_gpcc)/12))

  max_ind_gpcc <- brick(lapply(ind, function(x) calc(data_gpcc[[x:(x+11)]], which_max)))

  outfile <- paste0("$outdir_max", "/Max_annual_index_NDVI_v3.1.1_", 
                    "1981_2015_fin_year_GPCC_res.nc")
                    
  writeRaster(max_ind_gpcc, outfile, varname="month_index", 
              zunit="Years since 01-07-1981", overwrite=TRUE)

EOF

Rscript calc_annual_ind.R

rm calc_annual_ind.R


### Annual mean ###

#CRU
out_fin_mean_cru=$outdir_mean"/Mean_annual_NDVI_v3.1.1_1981_2016_fin_year_CRU_res.nc"
cdo -L selyear,1981/2016 -timselmean,12 $out_temp_cru $out_fin_mean_cru

#GPCC
out_fin_mean_gpcc=$outdir_mean"/Mean_annual_NDVI_v3.1.1_1981_2015_fin_year_GPCC_res.nc"
cdo -L selyear,1981/2015 -timselmean,12 $out_temp_gpcc $out_fin_mean_gpcc



### Annual total precipitation ###

#CRU
out_fin_pr_cru=$outdir_pr"/Total_annual_CRU_precipitation_1981_2016_fin_year.nc"
cdo selyear,1981/2016 -timselsum,12,6 $cru_file $out_fin_pr_cru

#GPCC
out_fin_pr_gpcc=$outdir_pr"/Total_annual_GPCC_precipitation_1981_2015_fin_year.nc"
cdo selyear,1981/2015 -timselsum,12,6 $gpcc_file $out_fin_pr_gpcc



### Annual total Esoil ###

#CRU
out_fin_esoil_cru=$esoil_dir"/Total_annual_ESoil_CRU_res_1982_2015_fin_year.nc"
cdo selyear,1981/2014 -timselsum,12,6 $out_temp_esoil_cru $out_fin_esoil_cru


#GPCC
out_fin_esoil_gpcc=$esoil_dir"/Total_annual_ESoil_GPCC_res_1982_2015_fin_year.nc"
cdo selyear,1981/2014 -timselsum,12,6 $out_temp_esoil_gpcc $out_fin_esoil_gpcc



### Annual total PET ###

#CRU
out_fin_pet_cru=$pet_dir"/Total_annual_PET_CRU_res_1982_2017_fin_year.nc"
cdo selyear,1981/2016 -timselsum,12,6 $out_temp_pet_cru $out_fin_pet_cru

#GPCC
out_fin_pet_gpcc=$pet_dir"/Total_annual_PET_GPCC_res_1982_2016_fin_year.nc"
cdo selyear,1981/2015 -timselsum,12,6 $out_temp_pet_gpcc $out_fin_pet_gpcc



#Remove temp files
#rm $out_temp_cru $out_temp_gpcc

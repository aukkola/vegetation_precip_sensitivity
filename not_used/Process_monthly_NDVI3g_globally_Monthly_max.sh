#!/bin/bash


#Process bimonthly NDVI3g files globally
#Average bimonthly values to monthly by taking max

INDIR="/srv/ccrc/data04/z3509830/LAI_precip_variability/Data_for_Patrick/Vegetation_indices/NDVI/Original_data"

#Set output directory
outdir=${INDIR}/../GIMMS3g_NDVI_global_1982_2015/

#Set start and end year
start_yr=1982
end_yr=2015


#Make output directory if doesn't exist
mkdir -p $outdir


#Cd to directory
cd $INDIR

#Get file names (without path so can modify filenames later)
files=`ls ndvi3g_geo_v1_*.nc4`


#First need to fix grids (this info was obtained with 
#command cdo griddes in.nc > grid.txt)

cat > ${outdir}/grid_info.txt << EOF
#
# gridID 1
#
gridtype  = latlon
gridsize  = 9331200
xsize     = 4320
ysize     = 2160
xname     = lon
yname     = lat
xfirst    = -179.958333333333
xinc      = 0.0833333333333333
yfirst    = 89.9583333333333
yinc      = -0.0833333333333286
EOF


#Loop through 6-monthly files
for F in $files
do

  #First select variable of interest (setgrid won't work otherwise)
  #(don't use path when creating file name so can be modified later)
  sel_grid="ndvi_${F%.*}.nc"
  cdo selname,ndvi $F $outdir/$sel_grid

  #Fix grid (otherwise subsequent commands won't work)
  out_grid="grid_fix_${sel_grid}"
  cdo setgrid,${outdir}/grid_info.txt $outdir/$sel_grid $outdir/$out_grid

  # #crop Australia
  # out_crop="Aus_${out_grid}"
  # cdo sellonlatbox,112.9,154,-43.8,-9 $out_grid $out_crop

  rm $outdir/$sel_grid # $out_grid


  ### Then extract quality control flags ###
  
  # #First select variable of interest (setgrid won't work otherwise)
  # qc_grid="qcflag_${F%.*}.nc"
  # cdo selname,percentile $F $outdir/$qc_grid
  # 
  # #Fix grid (otherwise subsequent commands won't work)
  # out_qc_grid="grid_fix_${qc_grid}"
  # cdo setgrid,$outdir/grid_info.txt $outdir/$qc_grid $outdir/$out_qc_grid
  # 
  # # #crop Australia
  # # out_crop_qc="Aus_${out_grid}"
  # # cdo sellonlatbox,112.9,154,-43.8,-9 $out_grid $out_crop_qc
  # 
  # rm $outdir/$qc_grid #$out_grid


  #Need to calculate QC flags from percentile variable
  #Arden's method:
  # flag_in = ncf1.variables['percentile']/2000.0
  # flag = np.floor(flag_in)
  
  # #First divide percentile by 2000
  # qc_in="div_${out_qc_grid}"
  # cdo -L expr,'percentile=floor(percentile/2000)' $outdir/$out_qc_grid $outdir/$qc_in
  # 
  # #Then make flags > 0 (non-observed) 1 and take time sum
  # #to get total number of good/bad flags for the 6-month period
  # binary_qc="total_bad_qc_${out_qc_grid}"
  # cdo timsum -gtc,0 $outdir/$qc_in $outdir/$binary_qc
  # 
  # #tidy up
  # rm $outdir/$qc_in


  ### Find monthly max from bimonthly time steps ###
  
  out_mean="Max_monthly_${out_grid}"
  
  cdo timselmax,2 $outdir/$out_grid $outdir/$out_mean
    
  #Recalculate QC flags as 0 (obs) or 1 (gapfilled)
  #Used to determine valid years later on

  
  ### Mask poor quality data ###

  # out_masked="Masked_${out_grid}"
  # 
  # #Mask out values > 1001 (original flag 1000 plus 1 as above, good quality data)
  # cdo div $out_grid -lec,1001 'nozero_'$out_qc_grid $out_masked
  
  
  #Tidy up
 rm $outdir/$out_grid #$outdir/$out_qc_grid #'nozero_'$out_qc_grid #R_fix_qcflag.R

done


#Not sure mergetime works because files not formatted properly, 
#using 'cdo cat' to be sure (think this should append correctly?)
#Merge time slices

ndvi_cat_out="NDVI_data_max_monthly_all_yrs.nc"
cdo -L cat ${outdir}/Max_monthly_*.nc ${outdir}/$ndvi_cat_out

# qc_cat_out="QC_flags_6_monthly_all_yrs.nc"
# cdo -L cat ${outdir}/total_bad_qc_*.nc ${outdir}/$qc_cat_out
# 

### Then average/sum 6-monthly to annual (take max value) ###

# #QC flags (add one to avoid dividing by zero below)
# qc_out="QC_flags_annual_all_yrs.nc"
# cdo -L expr,'percentile=percentile + 1' -timselsum,2 ${outdir}/$qc_cat_out ${outdir}/$qc_out
# 
#NDVI data
# ndvi_out="NVDI_data_annual_all_yrs_max.nc"
# cdo timselmax,2 ${outdir}/$ndvi_cat_out ${outdir}/$ndvi_out


### Mask using QC flags ###

#Then mask year when < 80% of biweekly were available (i.e. the number
#of bag QC flags was > 4. Except use 5 in the command below as added one above
#to QC flags to avoid zero division)

#First divide NDVI values by 10000 (strange gimms formatting)
out_temp="NDVI_data_monthly_all_yrs_div_max.nc"
cdo -L expr,'ndvi=ndvi/10000' ${outdir}/$ndvi_cat_out ${outdir}/$out_temp


#First mask out negative values (partly because missing value not
#not set correctly in original file)
out_no_neg="NDVI_data_monthly_all_yrs_no_negative_max.nc"
cdo -L div ${outdir}/$out_temp -gec,0 ${outdir}/$out_temp ${outdir}/$out_no_neg


#Then mask using QC flags (see above why using <=5 to mask)
# out_masked="NDVI_data_annual_all_yrs_no_negative_masked_max.nc"
# cdo -L div ${outdir}/$out_no_neg -lec,6 ${outdir}/$qc_out ${outdir}/$out_masked


### Select years ###

# First fix time stamps and units
out_masked_t_fixed="NDVI_data_monthly_all_yrs_masked_t_fixed_max.nc"
cdo -L settunits,years -settaxis,1982-01-01,00:00,1mon ${outdir}/$out_no_neg ${outdir}/$out_masked_t_fixed


#Finally select desired years
outfile_final="Global_NDVI3g_monthly_max_NDVI_${start_yr}_${end_yr}.nc"

cdo selyear,$start_yr/$end_yr ${outdir}/$out_masked_t_fixed ${outdir}/$outfile_final


#Tidy up
rm ${outdir}/$out_masked_t_fixed #${outdir}/$out_masked ${outdir}/$qc_cat_out 
rm ${outdir}/$ndvi_cat_out ${outdir}/$ndvi_out ${outdir}/$out_temp
rm ${outdir}/grid_info.txt
rm ${outdir}/Max_monthly_*.nc




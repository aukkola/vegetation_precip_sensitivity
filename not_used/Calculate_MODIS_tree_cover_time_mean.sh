
INDIR="/srv/ccrc/data04/z3509830/Nutrient_constraint/Land_cover_maps/tree_frac"

infile=$INDIR"/MOD44B.006_250m_aid0001.nc"

#Split to annual files for regridding
temp_dir=$INDIR"/annual_temp"
mkdir $temp_dir

### GPCC resolution ###

gpcc_file=("${INDIR}/../../Annual_obs_data_new_NDVI/precip/Total_annual_GPCC_"\
"precipitation_1982_2016_correct_hemisheres.nc")

years=(2000 2001 2002 2003 2004 2005 2006 2007 2008 2009 2010
2011 2012 2013 2014 2015 2016 2017)

for y in ${years[@]}
do
  
  #Percent tree cover  
  temp_tree="${temp_dir}/treecover_${y}_GPCC_res.nc"
  cdo remapcon2,$gpcc_file -selyear,$y -selname,Percent_Tree_Cover $infile $temp_tree


  #Percent Non-tree veg
  temp_notree="${temp_dir}/non-treecover_${y}_GPCC_res.nc"
  cdo remapcon2,$gpcc_file -selyear,$y -selname,Percent_NonTree_Vegetation $infile $temp_notree

  
done

#Merge tree files
cdo mergetime treecover*_GPCC_res.nc treecover_2000_2017_GPCC_res.nc

#Merge non-tree veg files
cdo mergetime non-treecover*_GPCC_res.nc non-treecover_2000_2017_GPCC_res.nc





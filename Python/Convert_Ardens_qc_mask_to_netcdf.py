#Convert python NDVI QC mask file to netcdf
#(QC from Burrell, A.L., Evans, J.P. & Liu, Y. (2018). 
#The impact of dataset selection on land degradation assessment. 
#ISPRS J. Photogramm. Remote Sens., 146, 22–37.)

import numpy as np
from netCDF4 import Dataset

#Set directory
indir="/srv/ccrc/data04/z3509830/Nutrient_constraint/"


### Arden's numpy mask file ###

mask_file = indir + '/QC_mask/' + 'QCmask_1101GLOBAL_1982_2015.npy'


mask = np.load(mask_file)


### Open global NDVI netcdf file for dimensions ###

ndvi_file = (indir + '/../LAI_precip_variability/Data_for_Patrick/' +
             'Vegetation_indices/NDVI/GIMMS3g_NDVI_global_1982_2015/' +
             'Global_NDVI3g_annual_max_NDVI_1982_2015.nc')


fh = Dataset(ndvi_file, mode='r')
lat = fh.variables['lat'][:]
lon = fh.variables['lon'][:]


#Check that dimensions match
if len(lat) != mask.shape[0] or len(lon) != mask.shape[1]:
    sys.exit("Dimensions don't match")

#Check that read north up 
if lat[0] < 0:
    sys.exit("NDVI needs flipping")


### Create netcdf file ###

out_file = indir + '/QC_mask/' + 'QCmask_1101GLOBAL_1982_2015.nc'

#Set missing value (use 0 when data masked)
miss_val = 0


# open a new netCDF file for writing.
ncfile = Dataset(out_file,'w', format="NETCDF4_CLASSIC")

# create the output data.
# create the x, y and time dimensions
ncfile.createDimension('lat', lat.shape[0])
ncfile.createDimension('lon', lon.shape[0])

# create variables
# first argument is name of variable, second is datatype, third is
# a tuple with the names of dimensions.

#Dimensions
longitude = ncfile.createVariable("lon",  'f8', ('lon',))
latitude  = ncfile.createVariable("lat",  'f8', ('lat',))

#Data variable
nc_data   = ncfile.createVariable('qc', 'i4',('lat','lon'), fill_value=miss_val)

# Set variable information
longitude.units = 'degrees_east'
latitude.units = 'degrees_north'

nc_data.long_name     = 'QC mask value'

# Write data to variable
longitude[:] = lon
latitude[:]  = lat

nc_data[:,:] = mask

# Close the file
ncfile.close()

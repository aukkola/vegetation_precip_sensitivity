get_median <- function(slopes, freq, breaks, x_vals, rsq=FALSE) {
  
  #Calculate median slope value
  median_value <- median(slopes, na.rm=TRUE)
  
  #Work out y- and x-value to place median on the plot
  
  #Interval median lies within
  int <- findInterval(median_value, breaks)
  
  #Find corresponding x-value
  #(this converts median value to the range used in plot (bin numbers, i.e. 1-13
  #rather than absolute slope values)
  
  if (rsq) {
    
    x_int <- breaks[int:(int+1)] + (x_vals[int] - breaks[int]) #use break values, but add an offset because x_vals 0.05 greater
      
    x_for_median <- median_value + 0.05

  } else {
    
    x_int <- x_vals[int:(int+1)] #use bin numbers (same as int here...)
    
    x_for_median <- int + (1- ((breaks[int+1] - median_value) / diff(breaks[int:(int+1)])))
    
  }

  lm <- lm(freq[int:(int+1)] ~ x_int)
  
  yval <-  lm$coefficients[2] * x_for_median + lm$coefficients[1]
  
  return(list(x=x_for_median, y=yval))
  
}


#-------------------------------


freqs <- function(x, breaks, area) {
  
  area_tot <- sum(area, na.rm=TRUE)
  
  density <- vector()
  
  for (b in 1:(length(breaks)-1)) {
    
    ind <- which(x >= breaks[b] & x < breaks[b+1])
    
    density[b] <- sum(area[ind], na.rm=TRUE) / area_tot
  }
  
  # h <- hist(x, breaks=breaks, plot=FALSE)
  # density <- h$counts / sum(h$counts)
  return(density)
}

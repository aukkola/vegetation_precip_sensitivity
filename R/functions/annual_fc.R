
#Calculate annual means when enough monthly data available
ann_mean <- function(data) {
  
  #Number of missing months
  no_NA <- length(which(is.na(data)))
  
  #If fewer than 20% of monthly values missing (i.e. 2 months),
  #calculate mean
  if (no_NA <= 2) {
    return(mean(data, na.rm=TRUE))
    
  } else {
    return(NA)
  }
}

#------------------

#Mask annual values when too many missing
mask_annual <- function(ann_data) {
  
  #Maximum number of allowed missing values (< 20% missing)
  allowed_missing <- floor(length(ann_data) * 0.2)
  
  #If too many missing, return NA
  if ( length(which(is.na(ann_data))) > allowed_missing) {
    
    return(rep(NA, length(ann_data)))
    
  } else {
    return(ann_data)
  }
}


#------------------

#Annual maximum

ann_max <-  function(data) {
  #Number of missing months
  no_NA <- length(which(is.na(data)))

  #If fewer than 20% of monthly values missing (i.e. 2 months),
  #calculate max
  if (no_NA <= 2) {
    
    return(max(data, na.rm=TRUE))
    
  } else {
    return(rep(NA))
  }

}  
  

#Index of annual maximum

ann_max_index <-  function(data, offset) {
  #Number of missing months
  no_NA <- length(which(is.na(data)))

  #If fewer than 20% of monthly values missing (i.e. 2 months),
  #calculate index
  if (no_NA <= 2) {

    return(which.max(data) + offset)

  } else {
    return(rep(NA))
  }

}








  
  
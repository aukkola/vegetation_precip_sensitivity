#Plots Figure S4

library(raster)
library(ncdf4)
library(maptools)

#clear R environment
rm(list=ls(all=TRUE))


#Set path 
path <- "/srv/ccrc/data04/z3509830/Nutrient_constraint"


#Source function
source(paste0(path, "/scripts/R/functions/slope_per_pixel.R"))
source(paste0(path, "/scripts/R/functions/add_raster_legend.R"))


### Read world shapefile ###

#Get world shapefile
world <- readShapePoly(paste0(path, "/../World_shapefile/World"))



#Precip datasets
precip_datasets <- c("GPCC")
end_yrs         <- c("2016")



#Use aridity limit (P/PET less than max_arid)
aridity_limit <- TRUE
max_arid      <- 0.65
min_arid      <- 0.05


#List metrics to use
metrics <- c("precip", "plant_avail_water", "aridity_index")


xlims <- list(c(0, 400), c(0, 2500), c(0, 3.5))

xlabs <- c("Mean monthly precipitation (mm)", "Mean annual net available water (mm)",
           "Mean annual moisture index (-)")

mains <- c("Optimal precipitation", "Net available water", "Moisture index")

#slope_units <- c("mm", "mm", "-")

multiplier <- c(10, 1000, 1)

lims <- list(c(-10000, -0.00025, -0.0002, -0.00015, -0.0001, -0.00005,  0,
                0.00005, 0.0001, 0.00015, 0.0002, 0.00025, 10000) * 100,
             
             c(-10000, -0.0002, -0.0001, -0.00006, -0.00005, -0.00003, -0.00001, 0,
                0.00001, 0.00003, 0.00005, 0.00006, 0.0001, 0.0002, 10000) * 1000,
             
             c(-10000, -0.0002, -0.0001, -0.00006, -0.00005, -0.00003, -0.00001, 0,
               0.00001, 0.00003, 0.00005, 0.00006, 0.0001, 0.0002, 10000) * 1000
             )


title <- c(expression("(×10"~"mm"^"-1"*")"), #"mm",
           expression("(×10"^"3"~"mm"^"-1"*")"),
           "(-)")


################
### Plotting ###
################


outdir <- paste0(path, "/Figures/Paper_figures/")
dir.create(outdir)


#Set up figure
png(paste0(outdir, "/FigureS4_spatial_and_temporal_slopes_other_measures_of_water_avail.png"),
    height=7, width=8, units="in", res=400)

par(mfcol=c(4,3))
#layout(matrix(1:3, nrow=3), widths=c(0.65, 1 ,1))

par(mai=c(0.55, 0.3, 0.2, 0.2))
par(omi=c(0, 0.3, 0.1, 0))


par(xpd=FALSE)



#Loop through metrics

for (m in 1:length(metrics)) {
  
  
  par(mai=c(0.0, 0.5, 0, 0.2))
  par(xpd=FALSE)
  
  
  ################
  ### Get data ###
  ################
  
  
  ### NDVI ###
  
  if (metrics[m] == "precip") {
    
    ndvi_file <- paste0(path, "/Annual_obs_data_new_NDVI/frac_cover/annual_max/", 
                        "Max_annual_frac_cover_v3.1.1_1982_",
                        end_yrs, "_correct_hemisheres_", precip_datasets, "_res.nc")
    
    
  } else {
    
    ndvi_file <- paste0(path, "/Annual_obs_data_new_NDVI/frac_cover/annual_mean/", 
                        "Mean_annual_frac_cover_v3.1.1_1982_",
                        end_yrs, "_correct_hemisheres_", precip_datasets, "_res.nc")
  }
    
  
  ndvi_data <- brick(ndvi_file)
  
  
  
  
  ### Water availablity metric ###
  
  if (metrics[m] == "precip") {
  
    rain_file <- paste0(path, "/Annual_obs_data_new_NDVI/precip/Total_optimal_lagged_annual_", 
                        precip_datasets, "_precipitation_1982_", end_yrs, "_correct_hemisheres.nc")
    
    
    #Also get lag
    
    lag_file <- paste0(path, "/Annual_obs_data_new_NDVI/precip/Optimal_precip_lag_in_months_", 
                       precip_datasets, "_1982_", end_yrs, "_correct_hemisheres.nc")
    
    lag <- raster(lag_file)
    
    
  } else if (metrics[m] ==  "plant_avail_water") {
    
    rain_file <- paste0(path, "/Annual_obs_data_new_NDVI/plant_avail_water/", 
                        "Total_annual_plant_available_water_",  precip_datasets, 
                        "_1982_2015_correct_hemisheres.nc")
    
    
  } else if (metrics[m] ==  "aridity_index") {
    
    rain_file <- paste0(path, "/Annual_obs_data_new_NDVI/aridity_index/", 
                        "Annual_aridity_index_P_over_PET_",  precip_datasets, 
                        "_1982_", end_yrs, "_correct_hemisheres.nc")
  }
  
    
  rain_data <- brick(rain_file)
  
  
  #If using lagged precip, convert to monthly precip
  if (metrics[m] == "precip") {
    
    rain_data <- rain_data / lag 
    
  }
  
  #Need to remove one layer from NDVI for net water avail
  #Esoil data finished in 2015
  if (metrics[m] ==  "plant_avail_water") { 
    
    ndvi_data <- ndvi_data[[1:nlayers(rain_data)]]
  }
  
  
  
  ### Aridity index ###
  
  if (aridity_limit) {
    
    #Aridity index
    
    arid_file <-  paste0(path, "/Regression_predictors/mean_aridity/", 
                         "/Mean_annual_aridity_index_P_over_PET_", precip_datasets, ".nc")
    
    aridity <- mask(raster(arid_file), ndvi_data[[1]])
    
  }  
  
  
  
  ##################################
  ## Calculate NDVI-precip slope ###
  ##################################
  
  
  #Calculate long-term mean values to find water limit
  mean_rain <- mask(mean(rain_data, na.rm=TRUE), aridity)
  mean_ndvi <- mask(mean(ndvi_data, na.rm=TRUE), aridity)
  
  
  #Get values
  mean_ndvi_vals <- as.vector(values(mean_ndvi))
  mean_rain_vals <- as.vector(values(mean_rain))
  
  arid_vals <- as.vector(values(aridity))
  arid_ind <- which(arid_vals <= max_arid & arid_vals >= min_arid)
  
  
  
  ### Calculate spatial slope ###
  
  
  #Calculate global slope for water-limited part
  global_lm <- lm(mean_ndvi_vals[arid_ind] ~ mean_rain_vals[arid_ind])
  
  
  
  #Check that precip and NDVI data match
  if (nlayers(rain_data) != nlayers(ndvi_data)) stop("Precip and NDVI data don't match")
  
  #Calculate slope per pixel
  slope_pixel <- calc(brick(list(rain_data, ndvi_data)), fun=slope_per_pixel)
  
  #Mask non water limited points
  #slope_pixel[mean_rain > breakpoint] <- NA
  
  
  if (aridity_limit) {
    
    slope_pixel[aridity > max_arid  | aridity < min_arid] <- NA
    
  }
  
  
  
  
  
  ####################
  ### Global slope ###
  ####################
  
  par(mai=c(0.25, 0.5, 0.2, 0.2))
  
  par(bty="o")
  
  
  #Density colours
  
  dens_cols  <- densCols(mean_rain_vals[arid_ind], mean_ndvi_vals[arid_ind], 
                         colramp=colorRampPalette(c("#99d8c9", "#006d2c")),
                         nbin=1000)
  
  ### Scatterplot ###
  
  #Plot all points in grey
  plot(mean_rain_vals, mean_ndvi_vals,  asp=NA, col="grey85", cex=0.1, pch=20, 
       xlim=xlims[[m]], ylim=range(mean_ndvi_vals, na.rm=TRUE), xlab="", ylab="",
       mgp=c(0,0.5,0), xaxs="i", yaxs="i")
  
  
  #Plot arid pixels
  points(mean_rain_vals[arid_ind], mean_ndvi_vals[arid_ind], col=dens_cols,
         cex=0.1, pch=20)
  
  #Redraw box 
  box()

  
  #Save global slope
  x_pred <- range(mean_rain_vals[arid_ind], na.rm=TRUE) #c(0, breakpoint)
  
  #Plot
  ypred <- global_lm$coefficients[2]*x_pred + global_lm$coefficients[1]
  
  lines(x_pred, ypred, col="grey30", lwd=3)
  
  
  #Add x label
  mtext(side=1, xlabs[m], line=2, xpd=NA, cex=0.7)
  
  #Add y label
  mtext(side=2, expression("Mean annual "*italic(f[c])*" (-)"), line=2, xpd=NA, cex=0.7)
  
  #Print equation
  text(x=xlims[[m]][2] - xlims[[m]][2]*0.5, y=0.15, paste0("y = ", 
                              sprintf("%1.4f", global_lm$coefficients[2]), "x + ",
                              round(global_lm$coefficients[1], 3)), cex=0.75, adj=0)
  
  #Print R-squared
  text(x=xlims[[m]][2] - xlims[[m]][2]*0.5, y=0.10, bquote("R"^{2}~"="~.(sprintf("%0.2f", 
                                              summary(global_lm)$r.squared))),
                                              cex=0.75, adj=0)
  
  legend("topleft", legend=c("spatial slope"), lty=c(1), 
         col="grey30", bty="n", lwd=c(3), cex=0.8)
  
  
  #main title
  if (metrics[m] == "precip") mtext(side=2, "Spatial slope", line=5, cex=0.8, font=2, xpd=NA)
  
  
  #Main title
  mtext(side=3, mains[m], line=1, cex=0.8, font=2, xpd=NA)
  
  
  
  ##############################
  ### Map of temporal slopes ###
  ##############################
  
  par(mai=c(0.3, 0, 0.3, 0))
  
  
  map_cols=colorRampPalette(c("#8c510a", "#d8b365", "#f6e8c3", 
                              "#e6f5d0", "#a1d76a", "#4d9221"))
  
  
  par(bty="n")
  
  #World outline
  plot(world, col="grey97", border="grey50") 
  
  
  # plot(slope_pixel[[1]], col=map_cols(length(lims)-1), breaks=lims,
  #      legend=FALSE, xaxt="n", yaxt="n", ylim=c(-65, 90), add=TRUE)
  
  image(slope_pixel[[1]] * multiplier[m], axes=F, ann=F, asp=1,
        col=map_cols(length(lims[[m]])-1), breaks=lims[[m]], add=TRUE)
  
  
  #Add legend
  add_raster_legend2(cols=map_cols(length(lims[[m]])-1), limits=lims[[m]][2:(length(lims[[m]])-1)], 
                     plot_loc=c(0.12,0.88, -0.14,-0.11), spt.cex=0.8, clip=TRUE,
                     main_title=title[m], adj_ticks=2.5)
  
  
  #Add title
  if (metrics[m] == "precip") mtext(side=2, "Temporal slope", line=1, cex=0.8, font=2, xpd=NA)
  
  
  
  
  ##################################
  ### Difference to global slope ###
  ##################################
  
  #Express as a percentage difference from global slope
  diff_to_global_slope <- ((slope_pixel[[1]] / global_lm$coefficients[2] ) -1) *100
  
  
  diff_lims <- c(-10000000, -100, -90, -80, -70, -60, -50,  
                 0, 100000)
  
  
  diff_cols <- c(colorRampPalette(c("#8c510a", "#d8b365", "#f6e8c3"))(length(diff_lims)-2),
                 "#a1d76a")
  
  
  
  #World outline
  plot(world, col="grey97", border="grey50") 
  
  
  image(diff_to_global_slope, axes=F, ann=F, asp=1,
        col=diff_cols, breaks=diff_lims, add=TRUE)
  
  # plot(diff_to_global_slope, col=diff_cols, breaks=diff_lims,
  #      legend=FALSE, xaxt="n", yaxt="n", ylim=c(-65, 90), add=TRUE)
  
  
  #Add legend
  if (metrics[m] == "plant_avail_water") add_raster_legend2(cols=diff_cols, limits=diff_lims[2:(length(diff_lims)-1)], 
                     plot_loc=c(0,1,-0.14,-0.11), spt.cex=0.8, main_title="(%)",
                     clip=TRUE, adj_ticks=2.5)
  
  
  #Add title
  if (metrics[m] == "precip") mtext(side=2, "Temporal—spatial", line=1, cex=0.8, font=2, xpd=NA)
  
  
  
  
  ##############################
  ### r-squared of obs slope ###
  ##############################
  
  
  
  #Set figure colours
  rsq_lims <- c(seq(0, 0.7, by=0.1), 1)
  
  map_cols <- colorRampPalette(c("#ffffcc", "#c7e9b4", "#7fcdbb", "#41b6c4",
                                 "#1d91c0", "#225ea8", "#0c2c84"))
  
  #World outline
  plot(world, col="grey97", lwd=0.8)
  
  #Map
  image(slope_pixel[[3]], axes=F, ann=F, asp=1,
        col=map_cols(length(rsq_lims)-1), breaks=rsq_lims, add=TRUE)
  
  
  #Add legend
  if (metrics[m] == "plant_avail_water") add_raster_legend2(cols=map_cols(length(rsq_lims)-1), 
                                                            limits=rsq_lims[2:(length(rsq_lims)-1)],
                    plot_loc=c(0,1,-0.14,-0.11), spt.cex=0.8, clip=TRUE,
                     main_title=expression("(-)"), adj_ticks=2.5, title.cex=0.6)
  
  
  #Add title
  if (metrics[m] == "precip") mtext(side=2, "R-squared", line=1, cex=0.8, font=2, xpd=NA)
  
  
  #Add outline again
  plot(world, add=TRUE, lwd=0.8, border="grey50")



  
  
}



dev.off()




mean(values(slope_pixel[[1]]), na.rm=T)

quantile(values(slope_pixel[[1]]), probs=c(0.5, 0.75), na.rm=T)



mean(values(slope_pixel[[3]]), na.rm=T)

quantile(values(slope_pixel[[3]]), probs=c(0.5, 0.75), na.rm=T)



mean(values(diff_to_global_slope), na.rm=T)




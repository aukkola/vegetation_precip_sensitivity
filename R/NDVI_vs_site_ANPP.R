#Plots figures 1 and S6

library(raster)
library(ncdf4)
library(maptools)

#clear R environment
rm(list=ls(all=TRUE))


#Set path 
path <- "/srv/ccrc/data04/z3509830/Nutrient_constraint/"



#Precip datasets
precip_datasets <- c("GPCC")
end_yrs         <- c("2016")



#Use aridity limit (P/PET less than max_arid)
aridity_limit <- TRUE
max_arid      <- 0.65
min_arid      <- 0.05



################
### Get data ###
################



### NDVI ###

ndvi_file <- paste0(path, "/Annual_obs_data_new_NDVI/frac_cover/annual_mean/Mean_annual_frac_cover_v3.1.1_1982_",
                    end_yrs, "_correct_hemisheres_", precip_datasets, "_res.nc")

ndvi_data <- brick(ndvi_file)



### Aridity index ###

if (aridity_limit) {
  
  #Aridity index
  
  arid_file <-  paste0(path, "/Regression_predictors/mean_aridity/", 
                       "/Mean_annual_aridity_index_P_over_PET_", precip_datasets, ".nc")
  
  aridity <- mask(raster(arid_file), ndvi_data[[1]])
  
}  




#################################
## Calculate mean annual NDVI ###
#################################


#Calculate long-term mean values to find water limit
mean_ndvi <- mask(mean(ndvi_data, na.rm=TRUE), aridity)


######################
### Site ANPP data ###
######################

anpp <- read.csv(paste0(path, "/Site_ANPP/Mean_annual_site_ANPP.csv"), header=TRUE)



#######################
### Site vs gridded ###
#######################



#Set up figure
png(paste0(path, "/Figures/Paper_figures//FigureXX_site_ANPP_vs_NDVI.png"),
    height=4, width=5, units="in", res=400)

par(mai=c(0.55, 0.1, 0.2, 0.1))
par(omi=c(0.3, 0.7, 0.1, 0))



### All sites ###

#Coordinates for extracting pixels
coords <- matrix(data=c(anpp$lon, anpp$lat), ncol=2, nrow=length(anpp$lat))


ndvi_for_sites <- extract(mean_ndvi, coords)


plot(anpp$ANPP, ndvi_for_sites, xlab="", 
     ylab="", pch=20, cex=0.8)


#X-lab
mtext(side=1, expression("Mean annual site ANPP"~"(g m"^2~"yr"^-1*")"), line=2.5)

#Y-lab
mtext(side=2,expression("Mean annual "*italic(f[c])*" (-)"), line=2.5)




### Add linear fit ###

#All sites

x <- anpp$ANPP

linear_fit <- lm(ndvi_for_sites ~ x)

summary_lm <- summary(linear_fit)

y <- summary_lm$coefficients[2] * x + summary_lm$coefficients[1]

lines(x, y)

#Get confidence intervals
xvals <- seq(min(x), max(x), length.out=length(anpp$ANPP))
confints <- predict(linear_fit, newdata = data.frame(x=xvals), interval = 'confidence',
                        level=0.95)

#Add linear regression with uncertainty
polygon(c(rev(xvals), xvals), c(rev(confints[,3]), confints[,2]), 
        col=adjustcolor("black", alpha.f=0.15), border=NA)



#Plot dryland sites in a different colour

#Mask out non-drylands
mean_ndvi[aridity > max_arid  | aridity < min_arid] <- NA

#Coordinates for extracting pixels
coords <- matrix(data=c(anpp$lon, anpp$lat), ncol=2, nrow=length(anpp$lat))

ndvi_for_sites <- extract(mean_ndvi, coords)

points(anpp$ANPP[which(!is.na(ndvi_for_sites))], ndvi_for_sites[which(!is.na(ndvi_for_sites))], 
       col="orange", pch=20, cex=0.8)


#Add R-squared
#Print R-squared

if(summary_lm$coefficients[8] > 0.0001) stop("fix p-value label")

text(x=950, y=0.15, bquote("R"^{2}~"="~.(round(summary_lm$r.squared, digits=2))~
                             ", p < 0.0001, n = "~.(length(x))),
     cex=0.8, adj=0)



summary(lm(ndvi_for_sites[which(!is.na(ndvi_for_sites))] ~
     anpp$ANPP[which(!is.na(ndvi_for_sites))]))



#Dryland sites

x <- anpp$ANPP[which(!is.na(ndvi_for_sites))]

linear_fit <- lm(ndvi_for_sites[which(!is.na(ndvi_for_sites))] ~ x)

summary_lm <- summary(linear_fit)

y <- summary_lm$coefficients[2] * x + summary_lm$coefficients[1]

lines(x, y, col="orange")

#Get confidence intervals
xvals <- seq(min(x), max(x), length.out=length(anpp$ANPP))
confints <- predict(linear_fit, newdata = data.frame(x=xvals), interval = 'confidence',
                    level=0.95)

#Add linear regression with uncertainty
polygon(c(rev(xvals), xvals), c(rev(confints[,3]), confints[,2]), 
        col=adjustcolor("orange", alpha.f=0.15), border=NA)




dev.off()

# 
# ### Dryland sites ###
# 
# #Mask out non-drylands
# mean_ndvi[aridity > max_arid  | aridity < min_arid] <- NA
# 
# #Coordinates for extracting pixels
# coords <- matrix(data=c(anpp$lon, anpp$lat), ncol=2, nrow=length(anpp$lat))
# 
# 
# ndvi_for_sites <- extract(mean_ndvi, coords)
# 
# 
# plot(anpp$ANPP[which(!is.na(ndvi_for_sites))], ndvi_for_sites[which(!is.na(ndvi_for_sites))], xlab="Site ANPP", ylab="GIMMS NDVI", main="Dryland sites")
# 
# 
# linear_fit <- lm(ndvi_for_sites ~ anpp$ANPP)
# 
# summary(linear_fit)
# 
# 
# 
# 
# 
# 
# 
# 
# lat <- -24.65
# lon <- 28.7
# 
# coords <- matrix(c(lon,lat), ncol=2, byrow=T)
# 
# extract(mean_ndvi, coords)
# 
# 


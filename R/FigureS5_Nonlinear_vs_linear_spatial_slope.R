#Calculates spatial and temporal NDVI-precip slopes using Sen slope method

library(raster)
library(maptools)
library(nlme)
library(broom)

#clear R environment
rm(list=ls(all=TRUE))


#Set path 
path <- "/srv/ccrc/data04/z3509830/Nutrient_constraint"


#Source function
source(paste0(path, "/scripts/R/functions/slope_per_pixel.R"))
source(paste0(path, "/scripts/R/functions/add_raster_legend.R"))


### Read world shapefile ###

#Get world shapefile
world <- readShapePoly(paste0(path, "/../World_shapefile/World"))



#Precip datasets
precip_datasets <- c("GPCC")
end_yrs         <- c("2016")



#Use aridity limit (P/PET less than max_arid)
aridity_limit <- TRUE
max_arid      <- 0.65
min_arid      <- 0.05



################
### Get data ###
################



### NDVI ###

ndvi_file <- paste0(path, "/Annual_obs_data_new_NDVI/frac_cover/annual_mean/Mean_annual_frac_cover_v3.1.1_1982_",
                    end_yrs, "_correct_hemisheres_", precip_datasets, "_res.nc")

ndvi_data <- brick(ndvi_file)




### Precip ###

rain_file <- paste0(path, "/Annual_obs_data_new_NDVI/precip/Total_annual_", precip_datasets, 
                    "_precipitation_1982_", end_yrs, "_correct_hemisheres.nc")

rain_data <- brick(rain_file)




### Aridity index ###

if (aridity_limit) {
  
  #Aridity index
  
  arid_file <-  paste0(path, "/Regression_predictors/mean_aridity/", 
                       "/Mean_annual_aridity_index_P_over_PET_", precip_datasets, ".nc")
  
  aridity <- mask(raster(arid_file), ndvi_data[[1]])
  
}  




##################################
## Calculate NDVI-precip slope ###
##################################


#Calculate long-term mean values to find water limit
mean_rain <- mask(mean(rain_data, na.rm=TRUE), aridity)
mean_ndvi <- mask(mean(ndvi_data, na.rm=TRUE), aridity)




#Get values
mean_ndvi_vals <- as.vector(values(mean_ndvi))
mean_rain_vals <- as.vector(values(mean_rain))

arid_vals <- as.vector(values(aridity))
arid_ind <- which(arid_vals <= max_arid & arid_vals >= min_arid)



### Calculate spatial slope ###


#Calculate global slope for water-limited part

#Need to remove NA for zyp.sen
no_na <- which(!is.na(mean_ndvi_vals[arid_ind]))


data = data.frame(ndvi = mean_ndvi_vals[arid_ind][no_na],
                  precip = mean_rain_vals[arid_ind][no_na])
                  


### Linear model ###
global_lm_linear <- lm(ndvi ~ precip, data=data) #precip))

#Only use precip below 800mm (where linear and non-linear start to diverge)
global_lm_linear_800 <- lm(ndvi ~ precip, data=data[which(data$precip < 800),])


### Fit non-linear curve to rain-ndvi relationship ###

#Estimate starting point for nonlinear model
a_start <- 0.1 #param a is the y value when x=0
b_start <- 2*log(2)/a_start #b is the decay rate

#Create non-linear model
global_lm_nonlinear <- nls(ndvi ~ a*precip/(b+precip), data=data, start=list(a=a_start,b=b_start))




# #Compare models (gets you AIC/BIC values)
# glance(global_lm)
# 
# glance(exp.mod)
# 


#################################
### Plot linear vs non-linear ###
#################################

outdir <- paste0(path, "/Figures/Paper_figures/")
dir.create(outdir)


#Set up figure
png(paste0(outdir, "/FigureSX_compare_linear_nonlinear_spatial_slopes.png"),
    height=3.5, width=4, units="in", res=400)

par(mfcol=c(1,1))
#layout(matrix(1:3, nrow=3), widths=c(0.65, 1 ,1))

par(mai=c(0.55, 0.5, 0.2, 0.2))
par(omi=c(0.1, 0.2, 0.1, 0.2))




#Predict ndvi values from models

### Linear ###

#All values
y_lin <- global_lm_linear$coefficients[2] * mean_rain_vals[arid_ind] + 
         global_lm_linear$coefficients[1]

#Precip below 800 mm
x_800 <- mean_rain_vals[arid_ind][which(mean_rain_vals[arid_ind] < 800)]
y_lin_800 <- global_lm_linear_800$coefficients[2] * x_800 + 
             global_lm_linear_800$coefficients[1]




### Non-linear ###

#First xreate x-value sequence (1 mm intervals)
rain_pred <- seq(0, max(mean_rain_vals[arid_ind], na.rm=TRUE), by=1)

#Then predict ndvi values
ndvi_pred <- predict(global_lm_nonlinear,list(precip=rain_pred))


### Plot ###

#First plot density scatter

#Density colours

dens_cols  <- densCols(mean_rain_vals[arid_ind], mean_ndvi_vals[arid_ind], 
                       colramp=colorRampPalette(c("#99d8c9", "#006d2c")),
                       nbin=1000)

### Scatterplot ###

#Plot all points in grey
# plot(mean_rain_vals, mean_ndvi_vals,  asp=NA, col="grey85", cex=0.1, pch=20, 
#      xlim=c(0,2500), ylim=range(mean_ndvi_vals, na.rm=TRUE), xlab="", ylab="",
#      mgp=c(0,0.5,0), xaxs="i", yaxs="i")


#Plot arid pixels
plot(mean_rain_vals[arid_ind], mean_ndvi_vals[arid_ind], col=dens_cols,
    cex=0.1, pch=20, xlab="", ylab="", asp=NA,
     mgp=c(0,0.5,0), xaxs="i", yaxs="i", cex.axis=0.7)

#Redraw box 
box()

#Add linear and non-linear fits  
lines(mean_rain_vals[arid_ind], y_lin, col="black", lwd=2) #all
lines(x_800, y_lin_800, col="grey50",lwd=2) #precip below 800 mm

lines(rain_pred, ndvi_pred, col="blue", lwd=2)


legend("bottomright", c("Linear (all points)", "Linear (P < 800 mm)", "Non-linear"),
       col=c("black", "grey50", "darkblue"), lty=c(1,1,1), bty="n", cex=0.5)


#Add x label
mtext(side=1, "Mean annual precipitation (mm)", line=2, xpd=NA, cex=0.7)

#Add y label
#mtext(side=2, "Mean annual frac. cover (-)", line=2, xpd=NA, cex=0.7)

mtext(side=2, expression("Mean annual "*italic(f[c])*" (-)"), line=2, xpd=NA, cex=0.7)


dev.off()


